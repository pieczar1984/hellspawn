
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HistoryComponent } from './content/history/history.component';
import { UniverseComponent } from './content/universe/universe.component';
import { GalleryComponent } from './content/gallery/gallery.component';

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/history',
    pathMatch: 'full',
  },
  {path: 'history', component: HistoryComponent},
  {path: 'universe', component: UniverseComponent},
  {path: 'gallery', component: GalleryComponent},
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavBarComponent,
    HistoryComponent,
    UniverseComponent,
    GalleryComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
 }
